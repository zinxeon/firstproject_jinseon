package com.likelion.controller;

import com.likelion.domain.response.Response;
import com.likelion.domain.dto.*;
import com.likelion.domain.response.UserJoinResponse;
import com.likelion.domain.response.UserLoginResponse;
import com.likelion.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    // 회원가입
    @PostMapping("/join")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest dto){
        UserDto userDto = userService.join(dto);
        UserJoinResponse response = new UserJoinResponse(userDto.getId(),userDto.getUserName());
        return Response.success(response);
    }

    // 로그인
    @PostMapping("/login")
    public Response<UserLoginResponse> login(@RequestBody UserLoginRequest dto){
        String token = userService.login(dto);
        UserLoginResponse response = new UserLoginResponse(token);
        return Response.success(response);
    }
}


