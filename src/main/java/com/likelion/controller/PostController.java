package com.likelion.controller;

import com.likelion.domain.dto.PostCreateRequest;
import com.likelion.domain.dto.PostDto;
import com.likelion.domain.response.PostCreateResponse;
import com.likelion.domain.response.Response;
import com.likelion.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/v1/posts")
@RequiredArgsConstructor
public class PostController {
    private final PostService postService;

    //포스트 작성
    @PostMapping
    public Response<PostCreateResponse> create(@RequestBody PostCreateRequest dto, @ApiIgnore Authentication authentication) {
        PostDto postDto = postService.create(dto, authentication.getName());
        PostCreateResponse response = new PostCreateResponse("Post 작성",postDto.getId());
        return Response.success(response);
    }

}
