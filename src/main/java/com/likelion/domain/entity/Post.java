package com.likelion.domain.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Post extends BaseEntity{

   @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String body;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public static Post of(String title, String body, User user) {
        Post post = new Post();
        post.setTitle(title);
        post.setBody(body);
        post.setUser(user);
        return post;
    }
}
