package com.likelion.service;

import com.likelion.domain.dto.PostCreateRequest;
import com.likelion.domain.dto.PostDto;
import com.likelion.domain.entity.Post;
import com.likelion.domain.entity.User;
import com.likelion.exception.AppException;
import com.likelion.exception.ErrorCode;
import com.likelion.repository.PostRepository;
import com.likelion.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PostService {
    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public PostDto create(PostCreateRequest dto,String userName) {
        //userName 못찾을때 에러
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, String.format("%s not founded", userName)));

        //저장
        Post savedPost = postRepository.save(Post.of(dto.getTitle(),dto.getBody(),user));

        return PostDto.builder()
                .id(savedPost.getId())
                .title(savedPost.getTitle())
                .body(savedPost.getBody())
                .build();
    }
}
