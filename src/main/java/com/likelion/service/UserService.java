package com.likelion.service;

import com.likelion.domain.entity.User;
import com.likelion.domain.dto.UserDto;
import com.likelion.domain.dto.UserJoinRequest;
import com.likelion.domain.dto.UserLoginRequest;
import com.likelion.exception.AppException;
import com.likelion.exception.ErrorCode;
import com.likelion.repository.UserRepository;
import com.likelion.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.token.secret}")
    private String key;
    private Long expireTimeMs= 1000*60*60l;

    // 회원가입
    public UserDto join(UserJoinRequest dto) {

        //userName 중복체크
        userRepository.findByUserName(dto.getUserName())
                .ifPresent(user -> {
                    throw new AppException(ErrorCode.USERNAME_DUPLICATION,"이미 존재하는 아이디입니다.");
                });

        String securityPassword = encoder.encode(dto.getPassword());
        User savedUser = userRepository.save(dto.toEntity(securityPassword));

        return UserDto.builder()
                .id(savedUser.getId())
                .userName(savedUser.getUserName())
                .password(savedUser.getPassword())
                .role(savedUser.getRole())
                .build();
    }

    // 로그인
    public String login(UserLoginRequest dto){
        //userName 없습니다
        User selectedUser = userRepository.findByUserName(dto.getUserName())
                .orElseThrow(()->new AppException(ErrorCode.USERNAME_NOT_FOUND,dto.getUserName()+"이 없습니다."));

        //비밀번호 잘못 입력
        if(!encoder.matches(dto.getPassword(),selectedUser.getPassword())){
            throw new AppException(ErrorCode.INVALID_PASSWORD, "패스워드를 잘못 입력하셨습니다.");
        }

        //토큰
        String token = JwtTokenUtil.createToken(selectedUser.getUserName(), key, expireTimeMs);
        return token;
    }

    public User getUserByUserName(String userName) {
        return userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ""));
    }

}


